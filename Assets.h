#pragma once

#include<SDL.h>
#include<SDL_image.h>
#include<string>
#include<iostream>

class Assets
{

private:
	/*  Map Assets         */
	SDL_Window* m_WindowMap;
	SDL_Surface* m_SurfaceMap;
	SDL_Texture* m_TextureMap;

	/*  Character Assets   */
	SDL_Surface* m_SurfaceCharacter;
	SDL_Texture* m_TextureCharacter;
	SDL_Rect m_CropCharacter;
	SDL_Rect m_PositionCharacter;
	SDL_Scancode keys[7]; 
	int m_CharacterTextureWidth;
	int m_CharacterFrameWidth, m_CharacterFrameHeight;


public:
	void Map(SDL_Renderer* m_UniversalRenderer, const std::string filePath);
	void MapDraw(SDL_Renderer* m_UniversalRenderer );

	void Character(SDL_Renderer* m_UniversalRenderer, int PositionX, int PositionY, int FrameX, int FrameY, SDL_Surface* m_SurfaceCharacter);
	void CharacterKeyBoard(float m_CharacterDelta, const Uint8* m_CharacterKeyState);
	void CharacterDraw(SDL_Renderer* m_UniversalRenderer);

	void Clean();

	Assets();
	~Assets();
};

