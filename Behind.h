#pragma once

#include<SDL.h>

class Behind
{

private:
	int m_PreviosTime = 0, m_CurrentTime = 0;
	float m_CharacterDelta = 0.0f;
public:
	float DeltaTime(float m_CharacterDelta);

	Behind();
	~Behind();
};
