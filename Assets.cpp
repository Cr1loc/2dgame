#include "Assets.h"



Assets::Assets()
{
}


Assets::~Assets()
{
}

void Assets::Character(SDL_Renderer* m_UniversalRenderer, int PositionX, int PositionY, int FrameX, int FrameY , SDL_Surface* m_SurfaceCharacter)
{
	m_SurfaceCharacter = IMG_Load("Assets/Character/Sprite-Right-Animation/All_Sprite_Right_Animation.png");

	m_TextureCharacter = SDL_CreateTextureFromSurface(m_UniversalRenderer, m_SurfaceCharacter);

	SDL_FreeSurface(m_SurfaceCharacter);

	SDL_QueryTexture(m_TextureCharacter, NULL, NULL, &m_CropCharacter.w, &m_CropCharacter.h);

	m_PositionCharacter.x = PositionX;
	m_PositionCharacter.y = PositionY;

	m_CharacterTextureWidth = m_CropCharacter.x;

	m_CropCharacter.w /= FrameX;
	m_CropCharacter.h /= FrameY;

	m_CharacterFrameWidth = m_PositionCharacter.w = m_CropCharacter.w;
	m_CharacterFrameHeight = m_PositionCharacter.h = m_CropCharacter.h;
}

void Assets::CharacterKeyBoard(float m_CharacterDelta, const Uint8* m_CharacterKeyState)
{
	keys[0] = SDL_SCANCODE_W; // Jump
	keys[1] = SDL_SCANCODE_A; // Run Lefts
	keys[2] = SDL_SCANCODE_S; // Crouch
	keys[3] = SDL_SCANCODE_D; // Run Right
	keys[4] = SDL_SCANCODE_SPACE; // Double Jump
	keys[5] = SDL_SCANCODE_O; // Attack
	keys[6] = SDL_SCANCODE_P; // Magic Attack

	float m_CharacterMoveSpeed = 200.0f;
	float m_CharacterFrameCounter = 0;
	bool isActive = true;

	/*               JUMP           */
	if (m_CharacterKeyState[keys[0]])
	{
		m_CropCharacter.y = m_CharacterFrameHeight * 6;
		//m_PositionCharacter.y += m_CharacterMoveSpeed * m_CharacterDelta;
	}
	/*               RUN LEFT       */
	else if (m_CharacterKeyState[keys[1]])
	{
		
		m_PositionCharacter.x -= m_CharacterMoveSpeed * m_CharacterDelta;
	}
	/*               CROUCH + RUN LEFT       */
	else if (m_CharacterKeyState[keys[2]] && m_CharacterKeyState[keys[1]])
	{
		
		m_PositionCharacter.x -= m_CharacterMoveSpeed * m_CharacterDelta;
	}
	/*               CROUCH + RUN RIGHT       */
	else if (m_CharacterKeyState[keys[2]] && m_CharacterKeyState[keys[3]])
	{
		
		m_PositionCharacter.x += m_CharacterMoveSpeed * m_CharacterDelta;
	}
	/*                RUN RIGHT       */
	else if (m_CharacterKeyState[keys[3]])
	{
		
		m_PositionCharacter.x += m_CharacterMoveSpeed * m_CharacterDelta;
	}
	/*                DOUBLE JUMP       */
	else if (m_CharacterKeyState[keys[4]])
	{
		
		m_PositionCharacter.y += m_CharacterMoveSpeed * m_CharacterDelta;
	}
	/*                ATTACK ON LEFT       */
	else if (m_CharacterKeyState[keys[5]] && m_CharacterKeyState[keys[1]])
	{
		
		m_PositionCharacter.x -= m_CharacterMoveSpeed * m_CharacterDelta;
	}
	/*                ATTACK ON RIGHT       */
	else if (m_CharacterKeyState[keys[5]] && m_CharacterKeyState[keys[3]])
	{
		
		m_PositionCharacter.x -= m_CharacterMoveSpeed * m_CharacterDelta;
	}
	/*               JUMP ATTACK ON LEFT       */
	else if (m_CharacterKeyState[keys[1]] && m_CharacterKeyState[keys[4]] && m_CharacterKeyState[keys[5]])
	{
		
		m_PositionCharacter.x -= m_CharacterMoveSpeed * m_CharacterDelta;
	}
	/*               JUMP ATTACK ON RIGHT       */
	else if (m_CharacterKeyState[keys[3]] && m_CharacterKeyState[keys[4]] && m_CharacterKeyState[keys[5]])
	{
	
		m_PositionCharacter.x -= m_CharacterMoveSpeed * m_CharacterDelta;
	}
	/*               MAGIC ATTACK ON LEFT       */
	else if (m_CharacterKeyState[keys[1]]  && m_CharacterKeyState[keys[6]])
	{
		
		m_PositionCharacter.x -= m_CharacterMoveSpeed * m_CharacterDelta;
	}
	/*               MAGIC ATTACK ON RIGHT      */
	else if (m_CharacterKeyState[keys[3]] && m_CharacterKeyState[keys[6]])
	{
		
		m_PositionCharacter.x -= m_CharacterMoveSpeed * m_CharacterDelta;
	}
	else
	{
		isActive = false;
	}

	if (isActive)
	{
		m_CharacterFrameCounter += m_CharacterDelta;
		if (m_CharacterFrameCounter >= 0.25f)
		{
			m_CharacterFrameCounter = 0;
			m_CropCharacter.x += m_CharacterFrameWidth;
			if (m_CropCharacter.x >= m_CharacterTextureWidth)
				m_CropCharacter.x = 0;
		}
	}
	else
	{
		m_CharacterFrameCounter = 0;
		m_CropCharacter.x = m_CharacterFrameWidth;
	}
}

void Assets::Map(SDL_Renderer* m_UniversalRenderer ,  const std::string filePath)
{
	m_SurfaceMap = IMG_Load(filePath.c_str());

	m_TextureMap = SDL_CreateTextureFromSurface(m_UniversalRenderer, m_SurfaceMap);

	SDL_FreeSurface(m_SurfaceMap);

	SDL_QueryTexture(m_TextureMap, NULL, NULL, NULL, NULL);

}

void Assets::Clean()
{
	SDL_DestroyWindow(m_WindowMap);
	SDL_DestroyTexture(m_TextureMap);
}

void Assets::MapDraw(SDL_Renderer* m_UniversalRenderer)
{
	SDL_RenderCopy(m_UniversalRenderer, m_TextureMap, NULL, NULL);
}

void Assets::CharacterDraw(SDL_Renderer* m_UniversalRenderer)
{
	SDL_RenderCopy(m_UniversalRenderer, m_TextureCharacter, &m_CropCharacter, &m_PositionCharacter);
}