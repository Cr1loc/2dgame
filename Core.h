#pragma once

#include "Assets.h"
#include "Behind.h"

class Core
{
private:
	SDL_Renderer* m_UniversalRender;
	SDL_Surface* m_UniversalSurface;
	SDL_Event m_Event;
	SDL_Window* m_Window;

	const Uint8* m_CharacterKeyState;
	float m_CharacterDelta;
	bool isRunning = true;
public:

	void Clean();
	void Start();
	Core();
	~Core();
};
