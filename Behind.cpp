#include "Behind.h"



Behind::Behind()
{
}


Behind::~Behind()
{
}

float Behind::DeltaTime(float m_CharacterDelta)
{
	m_PreviosTime = m_CurrentTime;
	m_CurrentTime = SDL_GetTicks();
	m_CharacterDelta = (m_CurrentTime - m_PreviosTime) / 1000.0f;

	return m_CharacterDelta;
}