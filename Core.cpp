#include "Core.h"



Core::Core()
{
	

}



Core::~Core()
{
}

void Core::Start()
{
	Assets* m_Assets = new Assets();
	Behind* m_Behind = new Behind();

	SDL_Init(SDL_INIT_EVERYTHING);

	m_Window = SDL_CreateWindow("Elementary", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, 640, 480, SDL_WINDOW_SHOWN);
	m_UniversalRender = SDL_CreateRenderer(m_Window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

	m_Assets->Map(m_UniversalRender , "Assets/Map/Start_Map.png");
	m_Assets->Character(m_UniversalRender, 100, 0, 12, 1 , m_UniversalSurface);

	while (isRunning)
	{

		m_Behind->DeltaTime(m_CharacterDelta);

		while (SDL_PollEvent(&m_Event) != 0)
		{
			switch (m_Event.type)
			{
			case SDL_QUIT:
				isRunning = false;
				break;
			default:
				break;
			}
		}

		m_CharacterKeyState = SDL_GetKeyboardState(NULL);
		m_Assets->CharacterKeyBoard(m_CharacterDelta, m_CharacterKeyState);
		SDL_RenderClear(m_UniversalRender);
		m_Assets->MapDraw(m_UniversalRender);
		m_Assets->CharacterDraw(m_UniversalRender);
		SDL_RenderPresent(m_UniversalRender);
	}
}